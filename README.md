# What is this?

To avoid repudiation, here are my previous OpenDKIM private keys. By my posting
these, you can't prove that I sent an email that I may or may not have sent in
the past.

See also [this post](https://blog.cryptographyengineering.com/2020/11/16/ok-google-please-publish-your-dkim-secret-keys/).
